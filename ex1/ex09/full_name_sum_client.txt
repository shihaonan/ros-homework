#!/usr/bin/env python3
import sys
import rospy

from service_pkg.srv import addition,additionResponse

def add_three_string_client(x, y, z):
    rospy.wait_for_service('add_three_string')
    add_three_string = rospy.ServiceProxy('add_three_string', addition)
    resp1 = add_three_string(x, y,z)
    print(resp1.result)

if __name__ == "__main__":
    x = str(sys.argv[1])
    y = str(sys.argv[2])
    z = str(sys.argv[3])
    add_three_string_client(x,y,z)

