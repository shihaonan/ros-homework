#!/usr/bin/env python3
import rospy
from service_pkg.srv import addition,additionResponse
def server_cb(req):
    return additionResponse(req.x + req.y+req.z)

if __name__ == "__main__":
    rospy.init_node('add_three_string_server')
    s=rospy.Service('add_three_string',addition,server_cb)
    rospy.spin()
