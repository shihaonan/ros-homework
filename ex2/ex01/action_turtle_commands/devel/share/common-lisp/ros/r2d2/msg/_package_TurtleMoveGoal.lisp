(cl:in-package r2d2-msg)
(cl:export '(TURTLE_TARGET_X-VAL
          TURTLE_TARGET_X
          TURTLE_TARGET_Y-VAL
          TURTLE_TARGET_Y
          TURTLE_TARGET_THETA-VAL
          TURTLE_TARGET_THETA
))