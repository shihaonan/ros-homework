// Generated by gencpp from file r2d2/TurtleMoveFeedback.msg
// DO NOT EDIT!


#ifndef R2D2_MESSAGE_TURTLEMOVEFEEDBACK_H
#define R2D2_MESSAGE_TURTLEMOVEFEEDBACK_H


#include <string>
#include <vector>
#include <memory>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>


namespace r2d2
{
template <class ContainerAllocator>
struct TurtleMoveFeedback_
{
  typedef TurtleMoveFeedback_<ContainerAllocator> Type;

  TurtleMoveFeedback_()
    : present_turtle_x(0.0)
    , present_turtle_y(0.0)
    , present_turtle_theta(0.0)  {
    }
  TurtleMoveFeedback_(const ContainerAllocator& _alloc)
    : present_turtle_x(0.0)
    , present_turtle_y(0.0)
    , present_turtle_theta(0.0)  {
  (void)_alloc;
    }



   typedef double _present_turtle_x_type;
  _present_turtle_x_type present_turtle_x;

   typedef double _present_turtle_y_type;
  _present_turtle_y_type present_turtle_y;

   typedef double _present_turtle_theta_type;
  _present_turtle_theta_type present_turtle_theta;





  typedef boost::shared_ptr< ::r2d2::TurtleMoveFeedback_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::r2d2::TurtleMoveFeedback_<ContainerAllocator> const> ConstPtr;

}; // struct TurtleMoveFeedback_

typedef ::r2d2::TurtleMoveFeedback_<std::allocator<void> > TurtleMoveFeedback;

typedef boost::shared_ptr< ::r2d2::TurtleMoveFeedback > TurtleMoveFeedbackPtr;
typedef boost::shared_ptr< ::r2d2::TurtleMoveFeedback const> TurtleMoveFeedbackConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::r2d2::TurtleMoveFeedback_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::r2d2::TurtleMoveFeedback_<ContainerAllocator> >::stream(s, "", v);
return s;
}


template<typename ContainerAllocator1, typename ContainerAllocator2>
bool operator==(const ::r2d2::TurtleMoveFeedback_<ContainerAllocator1> & lhs, const ::r2d2::TurtleMoveFeedback_<ContainerAllocator2> & rhs)
{
  return lhs.present_turtle_x == rhs.present_turtle_x &&
    lhs.present_turtle_y == rhs.present_turtle_y &&
    lhs.present_turtle_theta == rhs.present_turtle_theta;
}

template<typename ContainerAllocator1, typename ContainerAllocator2>
bool operator!=(const ::r2d2::TurtleMoveFeedback_<ContainerAllocator1> & lhs, const ::r2d2::TurtleMoveFeedback_<ContainerAllocator2> & rhs)
{
  return !(lhs == rhs);
}


} // namespace r2d2

namespace ros
{
namespace message_traits
{





template <class ContainerAllocator>
struct IsMessage< ::r2d2::TurtleMoveFeedback_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::r2d2::TurtleMoveFeedback_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::r2d2::TurtleMoveFeedback_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::r2d2::TurtleMoveFeedback_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::r2d2::TurtleMoveFeedback_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::r2d2::TurtleMoveFeedback_<ContainerAllocator> const>
  : FalseType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::r2d2::TurtleMoveFeedback_<ContainerAllocator> >
{
  static const char* value()
  {
    return "4d26027f0f230cc5b61fa6b93405965c";
  }

  static const char* value(const ::r2d2::TurtleMoveFeedback_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0x4d26027f0f230cc5ULL;
  static const uint64_t static_value2 = 0xb61fa6b93405965cULL;
};

template<class ContainerAllocator>
struct DataType< ::r2d2::TurtleMoveFeedback_<ContainerAllocator> >
{
  static const char* value()
  {
    return "r2d2/TurtleMoveFeedback";
  }

  static const char* value(const ::r2d2::TurtleMoveFeedback_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::r2d2::TurtleMoveFeedback_<ContainerAllocator> >
{
  static const char* value()
  {
    return "# ====== DO NOT MODIFY! AUTOGENERATED FROM AN ACTION DEFINITION ======\n"
"# Define a feedback message\n"
"float64 present_turtle_x\n"
"float64 present_turtle_y\n"
"float64 present_turtle_theta\n"
;
  }

  static const char* value(const ::r2d2::TurtleMoveFeedback_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::r2d2::TurtleMoveFeedback_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.present_turtle_x);
      stream.next(m.present_turtle_y);
      stream.next(m.present_turtle_theta);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct TurtleMoveFeedback_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::r2d2::TurtleMoveFeedback_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::r2d2::TurtleMoveFeedback_<ContainerAllocator>& v)
  {
    s << indent << "present_turtle_x: ";
    Printer<double>::stream(s, indent + "  ", v.present_turtle_x);
    s << indent << "present_turtle_y: ";
    Printer<double>::stream(s, indent + "  ", v.present_turtle_y);
    s << indent << "present_turtle_theta: ";
    Printer<double>::stream(s, indent + "  ", v.present_turtle_theta);
  }
};

} // namespace message_operations
} // namespace ros

#endif // R2D2_MESSAGE_TURTLEMOVEFEEDBACK_H
